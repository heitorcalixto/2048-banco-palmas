const fs = require('fs');
const file_encoding = 'utf8';

module.exports = {
    read_input_file_parameter: function() {
        return process.argv.slice(2)[0];
    },

    read_file: function(file_path){
        return fs.readFileSync(file_path, file_encoding);
    }
};
