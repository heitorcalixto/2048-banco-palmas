"use strict";

const assert = require('assert');
const move_left = require("../src/movers.js").move_left;
const it = require("mocha").it;
const describe = require("mocha").describe;


describe('movers.js', function() {
    describe('move_left', function() {
        it('deveria mover os elementos do grid para esquerda', function() {

            let grid = [
                ['0', '0', '0', '2'],
                ['0', '0', '0', '2'],
                ['0', '0', '0', '2'],
                ['0', '0', '0', '2']
            ];

            let expected_grid = [
                ['2', '0', '0', '0'],
                ['2', '0', '0', '0'],
                ['2', '0', '0', '0'],
                ['2', '0', '0', '0']
            ];


            let new_grid = move_left(grid);
            assert.equal(new_grid[0][0], expected_grid[0][0]);
        })


    });
});
