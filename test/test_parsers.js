"use strict";

const assert = require('assert');
var parse_raw_data_to_grid = require("../src/parsers.js").parse_raw_data_to_grid;
const it = require("mocha").it;
const describe = require("mocha").describe;


describe('parsers.js', function() {
    describe('parse_raw_data_to_grid', function() {
        it('deveria retornar uma lista de linhas', function() {

            let raw_data = '0 0 0 2\n2 0 0 2\n2 0 0 2\n2 0 0 2';
            let expected_grid = [
                ['0', '0', '0', '2'],
                ['2', '0', '0', '2'],
                ['2', '0', '0', '2'],
                ['2', '0', '0', '2']
            ];

            let grid = parse_raw_data_to_grid(raw_data);
            assert.equal(grid[0][0], expected_grid[0][0]);
            assert.equal(grid[1][2], expected_grid[1][2]);
        })


    });
});
